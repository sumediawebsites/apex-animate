[![Apex Animate](https://img.shields.io/badge/apex-animate-red.svg?style=flat)](https://www.sumedia.nl)
[![npm (scoped)](https://img.shields.io/npm/v/apex-animate?label=npm)](https://www.npmjs.com/package/apex-animate)
# Apex Animate #

> Trigger high-performance animations with a simple data-attribute 

___

## Installation
Install from npm
```
$ npm install apex-animate
```

___

## Usage
Import/require apex-animate in your file.
```javascript
import apexAnimate from 'apex-animate'

// or

const apexAnimate = require('apex-animate')
```
Use the data-attributes on HTML elements.
```html
<!-- Fade in animation -->
<div data-apex-animate="fade-in"></div>

<!-- Parallax animation -->
<div data-apex-animate="parallax"></div>
```
Optional: tweak options for the duration and delay (both in seconds).
```html
<!-- Fade in to top with extra options -->
<div data-apex-animate="fade-to-top" data-apex-animate-duration="2" data-apex-animate-delay=".2"></div>
```
The following animations are currently available:
```javascript
'fade-to-top'
'fade-to-right'
'fade-to-bottom'
'fade-to-left'
'fade-in'
'parallax'
```

___

## License
This project is available under the [MIT](https://opensource.org/licenses/mit-license.php) license.